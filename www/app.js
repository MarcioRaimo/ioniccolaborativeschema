var app = angular.module('start', [
  'ionic',
  'firebase',

  'fb',
  'login',
  'register',
  'utils',

  'app',
  'app.home',
]);

app.run(function ($ionicPlatform, $state, $rootScope, firebaseDatabaseSrvc, utilsSrvc) {
  $ionicPlatform.ready(function () {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }

    if (!window.cordova) {
      // Inicializa o plugin do Facebook para o browser
      window.facebookConnectPlugin.browserInit(350872055325297);
    } else {
    }

    if (localStorage.getItem('firebase:authUser:AIzaSyCktGpk3J67UIo3ZD30ftzrvVp2n43clYU:[DEFAULT]')) {
      utilsSrvc.showLoading();
      console.log(JSON.parse(localStorage.getItem('firebase:authUser:AIzaSyCktGpk3J67UIo3ZD30ftzrvVp2n43clYU:[DEFAULT]')));
      var temp = JSON.parse(localStorage.getItem('firebase:authUser:AIzaSyCktGpk3J67UIo3ZD30ftzrvVp2n43clYU:[DEFAULT]'));
      utilsSrvc.setRootScopeItem('facebookAcessToken', temp.stsTokenManager.accessToken)
      firebaseDatabaseSrvc.getUser(temp.uid).then(function (user) {
        utilsSrvc.setBothLocalStorageAndRootScopeItem('user', user);
        utilsSrvc.hideLoading();
        $state.go('app.home');
      });
    } else {
      $state.go('login');
    }

  });
});

app.config(function ($urlRouterProvider) {
  $urlRouterProvider.otherwise('/app');
});