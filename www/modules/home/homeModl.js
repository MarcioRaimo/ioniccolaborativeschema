angular.module('app.home', []).config(function($stateProvider){
    $stateProvider.state('app.home', {
        url: '/home',
        templateUrl: 'modules/home/home.html',
        controller: 'homeCtrl',
        controllerAs: 'vm'
    });
});