angular.module('register').controller('registerCtrl', function (registerSrvc) {

    var vm = this;

    vm.register = function () {
        if (vm.password === vm.passwordCheck) {
            registerSrvc.register({
                displayName: vm.displayName,
                email: vm.email,
                password: vm.password
            });
        } else {
            alert('Algo de errado não está certo! D:');
        }
    }

});