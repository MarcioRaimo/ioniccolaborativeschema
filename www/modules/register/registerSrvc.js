angular.module('register').service('registerSrvc', function ($state,
    utilsSrvc,
    firebaseAuthSrvc) {

    function register(userInfo) {
        utilsSrvc.showLoading();
        firebaseAuthSrvc.createUserWithEmailAndPassword(userInfo).then(function () {
            $state.go('app.home');
        }).catch(function (error) {
            utilsSrvc.hideLoading();
            console.error(error);
        });
    }

    return {
        register: register
    }

});