angular.module('register', []).config(function($stateProvider){
    $stateProvider.state('register', {
        url: '/register',
        templateUrl: 'modules/register/register.html',
        controller: 'registerCtrl',
        controllerAs: 'vm'
    });
});