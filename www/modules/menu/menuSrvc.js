angular.module('app').service('menuSrvc', function ($state,
    firebaseAuthSrvc,
    utilsSrvc) {

    function signOut(){
        utilsSrvc.showLoading();
        firebaseAuthSrvc.signOut().then(function(){
            $state.go('login');
        }).catch(function(error){
            utilsSrvc.hideLoading();
            console.error(error);
        });
    }

    return{
        signOut:signOut
    };
});