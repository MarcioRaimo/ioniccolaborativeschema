angular.module('app').controller('menuCtrl', function($ionicSideMenuDelegate,
    menuSrvc){

    var vm = this;

    vm.signOut = function (){
        $ionicSideMenuDelegate.toggleLeft();
        menuSrvc.signOut();
    }

});