angular.module('app', []).config(function($stateProvider){
    $stateProvider.state('app', {
        url: '/app',
        templateUrl: 'modules/menu/menu.html',
        controller: 'menuCtrl',
        controllerAs: 'vm'
    });
});