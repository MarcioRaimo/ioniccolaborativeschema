angular.module('utils').service('utilsSrvc', function ($ionicLoading,
    $rootScope) {

    /**
     * Mostra um loading personalizado
     */
    function showLoading() {
        $ionicLoading.show({
            template: 'Carregando dados</br><ion-spinner icon="dots"></ion-spinner>',
            hideOnStateChange: true
        });
    }

    /**
     * Esconde o loading
     */
    function hideLoading() {
        $ionicLoading.hide();
    }

    ////////// ---------- ////////// ---------- ////////// ---------- ////////// ---------- ////////// ----------

    /**
     * Salva algo '[data]' no $rootScope e localStorage na propriedade '[property]'
     * 
     * @param {string} property 
     * @param {any} data 
     */
    function setBothLocalStorageAndRootScopeItem(property, data) {
        setLocalStorageItem(property, data);
        setRootScopeItem(property, data);
    }

    /**
     * Salva algo '[data]' no $rootScope na propriedade '[property]'
     * 
     * @param {string} property 
     * @param {any} data 
     */
    function setRootScopeItem(property, data) {
        $rootScope[property] = data;
        console.log('$rootScope', $rootScope);
    }

    /**
     * Salva algo '[data]' no localStorage na propriedade '[property]'
     * 
     * @param {string} property 
     * @param {any} data 
     */
    function setLocalStorageItem(property, data) {
        localStorage.setItem(property, JSON.stringify(data));
        console.log('localStorage', localStorage);
    }

    /**
     * Pega algo [property] do localStorage
     * 
     * @param {string} property 
     */
    function getLocalStorageItem(property) {
        return JSON.parse(localStorage.getItem(property));
    }

    /**
     * Limpa o localStorage
     */
    function cleanLocalStorage() {
        localStorage.clear();
        console.log('localStorage', localStorage);
    }

    ////////// ---------- ////////// ---------- ////////// ---------- ////////// ---------- ////////// ----------

    function getUserToken() {
        var low = "abcdefghijklmnopqrstuvyxwz0123456789".split("");
        var high = "ABCDEFGHIJKLMNOPQRSTUVYXWZ".split("");
        var special = "!@#$%&".split("");
        var token = "";
        token += high[Math.floor(Math.random() * 26)];
        token += low[Math.floor(Math.random() * 36)];
        token += low[Math.floor(Math.random() * 36)];
        token += low[Math.floor(Math.random() * 36)];
        token += special[Math.floor(Math.random() * 6)];
        return token;
    }

    return {
        showLoading: showLoading,
        hideLoading: hideLoading,
        setBothLocalStorageAndRootScopeItem: setBothLocalStorageAndRootScopeItem,
        getLocalStorageItem: getLocalStorageItem,
        setRootScopeItem: setRootScopeItem,
        setLocalStorageItem: setLocalStorageItem,
        cleanLocalStorage: cleanLocalStorage,
        getUserToken: getUserToken
    }

})