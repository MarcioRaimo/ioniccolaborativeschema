angular.module('fb').service('firebaseDatabaseSrvc', function (firebaseFctr,
    firebaseMessagingSrvc,
    utilsSrvc) {

    var database = firebaseFctr.database;

    /**
     * Busca um usuário no banco de dados e retorna as suas informações
     * 
     * @param {string} userId 
     */
    function getUser(userId) {
        return firebaseMessagingSrvc.getToken().then(function (token) {
            console.log('token', token);
            return updateUser(userId, { messagingToken: token }).then(function () {
                return database.ref('users/' + userId).once('value').then(function (user) {
                    console.log('user', user.val());
                    return user.val();
                });
            })
        })
    }

    /**
     * Cria um usuário baseado no Facebook
     * 
     * @param {Object} userInfo
     * @param {string} userInfo.email
     * @param {string} userInfo.password 
     * @param {string} userInfo.displayName
     */
    function facebookRegister(user) {
        return firebaseMessagingSrvc.getToken().then(function (token) {
            var temp = {
                userToken: utilsSrvc.getUserToken(),
                messagingToken: token,
                displayName: user.displayName,
                email: user.email,
                photoURL: user.photoURL,
                id: user.uid,
                provider: 'facebook'
            };
            return database.ref('users/' + user.uid).set(temp).then(function () {
                utilsSrvc.setBothLocalStorageAndRootScopeItem('user', temp);
            }).catch(function (error) {
                console.error(error);
            });
        }).catch(function (error) {
            console.error(error);
        })
    }

    /**
     * Cria um usuário baseado em e-mail e senha
     * 
     * @param {Object} userInfo
     * @param {string} userInfo.email
     * @param {string} userInfo.password 
     * @param {string} userInfo.displayName
     */
    function emailRegister(userInfo) {
        return firebaseMessagingSrvc.getToken().then(function (token) {
            var temp = {
                userToken: utilsSrvc.getUserToken(),
                messagingToken: token,
                displayName: userInfo.displayName,
                email: userInfo.email,
                provider: 'email',
                id: userInfo.uid
            };
            return database.ref('users/' + temp.uid).set(temp).then(function () {
                utilsSrvc.setBothLocalStorageAndRootScopeItem('user', temp);
            }).catch(function (error) {
                console.error(error);
            });
        }).catch(function (error) {
            console.error(error);
        })
    }

    /**
     * Faz a atualização de um usuário
     * 
     * @param {String} userId 
     * @param {Object} data 
     */
    function updateUser(userId, data) {
        return database.ref('users/' + userId).update(data);
    }

    return {
        getUser: getUser,
        facebookRegister: facebookRegister,
        emailRegister: emailRegister
    }

});