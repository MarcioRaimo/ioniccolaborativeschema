angular.module('fb').service('firebaseAuthSrvc', function ($q,
    $rootScope,
    firebaseFctr,
    firebaseDatabaseSrvc,
    utilsSrvc) {

    var facebook = window.facebookConnectPlugin;
    var auth = firebaseFctr.auth;

    /**
     * Cria um usuário baseado em e-mail e senha
     * 
     * @param {Object} userInfo
     * @param {string} userInfo.email
     * @param {string} userInfo.password 
     * @param {string} userInfo.name
     * @return {Object} usuário criado
     */
    function createUserWithEmailAndPassword(userInfo) {
        return auth.createUserWithEmailAndPassword(userInfo.email, userInfo.password).then(function (createdUser) {
            console.log('usuario criado', createdUser);
            userInfo.uid = createdUser.uid;
            return firebaseDatabaseSrvc.emailRegister(userInfo);
        });
    }

    /**
     * Autentica o usuário com um e-mail e senha
     * 
     * @param {string} email 
     * @param {string} password 
     */
    function signInWithEmailAndPassword(email, password) {
        return auth.signInWithEmailAndPassword(email, password).then(function (userLogged) {
            return firebaseDatabaseSrvc.getUser(userLogged.uid).then(function (userSnapshot) {
                utilsSrvc.setBothLocalStorageAndRootScopeItem('user', userSnapshot);
            });
        });
    }

    /**
     * Faz login com o facebook
     */
    function facebookSignIn() {
        var q = $q.defer();
        // Pegando o status do usuário com o Facebook
        return facebookGetLoginStatus().then(function (userStatus) {
            if (userStatus.status === 'connected') {
                console.log('conectado');
                return facebookLogged(userStatus);
            } else {
                console.log('não conectado');
                return facebookNotLogged().then(function () {
                    return facebookGetLoginStatus().then(function (userStatus) {
                        return facebookLogged(userStatus);
                    })
                })
            }
        }).catch(function (error) {
            console.error(error);
            q.reject(error);
        });
    }

    /**
     * Caso o usuário esteja autenticado pelo Facebook
     */
    function facebookLogged(userStatus) {
        var q = $q.defer();
        // Criando uma credencial no Firebase baseado no token fornecido pelo Facebook
        auth.signInWithCredential(firebase.auth.FacebookAuthProvider.credential(userStatus.authResponse.accessToken)).then(function (userLogged) {
            console.log('signInWithCredential');
            // Pega o usuário na base de dados
            firebaseDatabaseSrvc.getUser(userLogged.uid).then(function (userSnapshot) {
                console.log('snapshot');
                if (userSnapshot) {
                    // Caso o usuário exista
                    console.log('tem usuario');
                    utilsSrvc.setBothLocalStorageAndRootScopeItem('user', userSnapshot);
                    q.resolve();
                } else {
                    // Caso o usuário não exista, cria um usuário
                    console.log('nao tem usuario');
                    firebaseDatabaseSrvc.facebookRegister(userLogged).then(function () {
                        console.log('salvou usuario');
                        q.resolve();
                    });
                }
            });
        });
        return q.promise;
    }

    /**
     * Caso o usuário não esteja autenticado pelo Facebook
     */
    function facebookNotLogged() {
        var q = $q.defer();
        facebook.login([], function () {
            q.resolve();
        }, function (error) {
            console.error(error);
            q.reject(error);
        });
        return q.promise;
    }

    /**
     * Pega o status do usuário com o serviço do Facebook
     */
    function facebookGetLoginStatus() {
        var q = $q.defer();
        facebook.getLoginStatus(function (userStatus) {
            q.resolve(userStatus);
        }, function (error) {
            console.error(error);
            q.reject(error)
        });
        return q.promise
    }

    /**
     * Faz a desautenticação do usuário
     */
    function signOut() {
        if ($rootScope.user.provider === 'email') {
            return emailLogout();
        } else {
            return facebookLogout();
        }
    }

    /**
     * Faz a desautenticação do usuário do Firebase
     */
    function emailLogout() {
        return auth.signOut().then(function () {
            utilsSrvc.cleanLocalStorage();
        }).catch(function (error) {
            console.error(error);
        });
    }

    /**
     * Faz a desautenticação do usuário do Facebook e Firebase
     */
    function facebookLogout() {
        var q = $q.defer();
        var temp = $rootScope.user.accessToken;
        facebookGetLoginStatus().then(function (response) {
            facebook.logout(function (response) {
                auth.signOut().then(function () {
                    utilsSrvc.cleanLocalStorage();
                    q.resolve();
                }).catch(function (error) {
                    console.error(error);
                    q.reject(error);
                });
            }, function (error) {
                console.error(error);
                q.reject(error)
            });
        })
        return q.promise;
    }

    return {
        createUserWithEmailAndPassword: createUserWithEmailAndPassword,
        facebookSignIn: facebookSignIn,
        signInWithEmailAndPassword: signInWithEmailAndPassword,
        signOut: signOut,
    }

});