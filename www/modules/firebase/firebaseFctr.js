angular.module('fb').factory('firebaseFctr', function () {
    var config = {
        apiKey: "AIzaSyCktGpk3J67UIo3ZD30ftzrvVp2n43clYU",
        authDomain: "teste-69ab5.firebaseapp.com",
        databaseURL: "https://teste-69ab5.firebaseio.com",
        projectId: "teste-69ab5",
        storageBucket: "teste-69ab5.appspot.com",
        messagingSenderId: "440909194450"
    };
    
    // Inicializa o banco de dados
    firebase.initializeApp(config);

    return {
        auth: firebase.auth(),
        database: firebase.database(),
        messaging: firebase.messaging(),
        storage: firebase.storage()
    }
});