angular.module('fb').service('firebaseMessagingSrvc', function (firebaseFctr, $q) {

    var messaging = firebaseFctr.messaging;

    function getToken() {
        var q = $q.defer();
        if (!window.cordova) {
            q.resolve(null)
        } else {
            FCMPlugin.getToken(function (token) {
                q.resolve(token)
            }, function (error) {
                console.error(error);
                q.reject(error)
            });
        }
        return q.promise;
    }

    return {
        getToken: getToken
    }

});