angular.module('login', []).config(function($stateProvider){
    $stateProvider.state('login', {
        url: '/login',
        templateUrl: 'modules/login/login.html',
        controller: 'loginCtrl',
        controllerAs: 'vm'
    });
});