angular.module('login').service('loginSrvc', function ($state,
    $ionicLoading,
    $rootScope,
    firebaseAuthSrvc,
    utilsSrvc) {

    /**
     * Autentica o usuário com e=mail e senha
     * 
     * @param {string} email 
     * @param {string} password 
     */
    function signInWithEmailAndPassword(email, password) {
        utilsSrvc.showLoading();
        firebaseAuthSrvc.signInWithEmailAndPassword(email, password).then(function () {
            console.log('LOGGADO!');
            $state.go('app.home');
        }).catch(function (error) {
            utilsSrvc.hideLoading();
            console.error('Algo deu errado na autenticação "/', error);
        });
    }

    /**
     * Faz a autenticação do usuário através do Facebook
     */
    function facebookSignIn() {
        utilsSrvc.showLoading();
        firebaseAuthSrvc.facebookSignIn().then(function () {
            console.log('LOGGADO COM O FACEBOOK!');
            $state.go('app.home');
        }).catch(function (error) {
            utilsSrvc.hideLoading();
            console.error('ALGO DEU ERRADO MANOLO =(', error);
        });
    }

    return {
        facebookSignIn: facebookSignIn,
        signInWithEmailAndPassword: signInWithEmailAndPassword
    };
});