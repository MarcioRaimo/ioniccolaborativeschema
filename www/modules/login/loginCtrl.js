angular.module('login').controller('loginCtrl', function ($state,
    loginSrvc) {

    var vm = this;

    vm.signIn = function () {
        loginSrvc.signInWithEmailAndPassword(vm.email, vm.password);
    };

    vm.facebookSignIn = function () {
        loginSrvc.facebookSignIn();
    }

    vm.register = function (email, password) {
        $state.go('register');
    };

    vm.clean = function () {
        localStorage.clear();
    };

});